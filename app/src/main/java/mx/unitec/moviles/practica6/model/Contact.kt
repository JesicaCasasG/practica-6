package mx.unitec.moviles.practica6.model

data class Contact(val name: String, val phone: String, val email: String) {
}